/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef TOP_H
#define TOP_H

#include <systemc.h>

using namespace std;

//template <typename TRAITS >
  class top: public sc_module {
 public:

    sc_in<bool> clk;
    sc_in<bool> rst;

    int A[2];
    typedef sc_uint<14> word;
   
    sc_in<sc_uint<32> > p_input;
    sc_out<sc_uint<32> > p_output;

    int func_x( int x );
    int func_y( int x );


    void run () {
      p_output = 0;
      wait();
      while ( true ) { 
	int d_in;
	int  d_out;
	d_in = p_input.read();
	d_out = func_x(d_in);
	p_output.write(d_out);
	wait(); 
      } 
    }


 public:
    SC_HAS_PROCESS(top);
    top(sc_module_name name);


};
#endif
