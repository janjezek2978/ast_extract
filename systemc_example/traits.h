/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef TRAITS_H
#define TRAITS_H

template <typename BOOL_T, typename IN_TYPE >
struct traits {
  static const BOOL_T not_true = ! true;
  typedef int out_type;
  typedef IN_TYPE in_type; 
};

#endif
