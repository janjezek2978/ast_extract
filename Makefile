
#MAIN_SRC_PATH =$(shell readlink -e ./systemc_example ) 
MAIN_SRC_PATH =$(shell readlink -e . ) 

PLUGIN=ast_extract.so
SOURCES= src/typename.cpp src/declbase.cpp src/typedecl.cpp src/xmlwriter.cpp src/modifyexpression.cpp src/callexpression.cpp src/loopexpression.cpp src/expressionbase.cpp src/arraytype.cpp src/pointertype.cpp src/nativecpptype.cpp  src/typebase.cpp src/functiondecl.cpp src/vardecl.cpp  src/templateargvalue.cpp src/ast_extract.cpp src/classtype.cpp src/analyzer_pass.cpp  $(END)

include ./Makefile.common


test:	$(PLUGIN)
	$(CXXPLUGIN) -O0 -fplugin=./ast_extract.so -fplugin-arg-ast_extract-src_path=$(MAIN_SRC_PATH)  -include sc_module_export.h  -I${SYSTEMC_HOME}/include  -I./systemc_example -c  systemc_example/top.cpp   -o /dev/null 
	$(CXXPLUGIN) -O0 -fplugin=./ast_extract.so -fplugin-arg-ast_extract-src_path=$(MAIN_SRC_PATH)  -include sc_module_export.h  -I${SYSTEMC_HOME}/include  -I./systemc_example -c  systemc_example/main.cpp   -o /dev/null 

diff:
	diff systemc_example/top.cpp.xml xml_ref/top.cpp.xml
	diff systemc_example/main.cpp.xml xml_ref/main.cpp.xml





