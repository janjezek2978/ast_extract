/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#include "declbase.h"



DeclBase::DeclBase ( tree & _decl): decl(_decl) {
  name = "NotNamed";
  full_name = name;
  if ( TREE_CODE(decl ) == RESULT_DECL ) {
    full_name = "return";
    name = full_name;
  } else {
    name = lang_hooks.decl_printable_name (decl,0);
  }
  decl_line = DECL_SOURCE_LINE(decl);
  decl_col  = DECL_SOURCE_COLUMN(decl);
  decl_file = DECL_SOURCE_FILE (decl);
  scope_str = decl_scope (decl);

}

void DeclBase::print( ) {
  inform ( DECL_SOURCE_LOCATION (decl),"%s Declaration at this location.",full_name.c_str());
}


string DeclBase::get_full_name() {
  return full_name;
}


string DeclBase::get_name() {
  return name;
}

string DeclBase::get_scope() {
  return scope_str;
}


unsigned int DeclBase::get_decl_line(){ return decl_line; }

unsigned int DeclBase::get_decl_col(){ return decl_col; }

string DeclBase::get_decl_file(){ return decl_file; }


