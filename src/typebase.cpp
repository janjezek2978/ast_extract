/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "typebase.h"



TypeBase::TypeBase ( tree & _type ): type(TYPE_MAIN_VARIANT (_type)) {

  

}


string TypeBase::get_full_name() {
  return full_name;
}


string TypeBase::get_name() {
  return name;
}

string TypeBase::get_scope() {
  return scope;
}


void TypeBase::print() {
   cout << name  << endl;
}


unsigned int TypeBase::get_line(){ return line; }

unsigned int TypeBase::get_col(){ return col; }

string TypeBase::get_file(){ return file; }

