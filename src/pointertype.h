/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_PTRTYPE_
#define _H_PTRTYPE_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "typebase.h"


using namespace std;

class PointerType: public TypeBase {
public:


PointerType ( tree & _type );
TypeBase::t_variant get_variant();
void dump_xml( xmlwriter & xmlw );


private:
 int ptr_level;
PointerType();
};


inline PointerType * is_PointerType(TypeBase * n) {
  PointerType * rtn;
  if (  (n->get_variant() == TypeBase::V_PointerType) && ( n != NULL) )
    rtn = static_cast<PointerType*>(n);
  else
    rtn = NULL;
  return rtn;
}

#endif
