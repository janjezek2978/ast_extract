/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_FIND_MOD_PASS_
#define _H_FIND_MOD_PASS_


#include <iostream>
#include <assert.h>

#include "gcc-plugin.h"

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"

#include "tree.h"

#include "function.h"
#include "basic-block.h"
#include "tree-ssa-alias.h"

#include "gimple-expr.h"
#include "coretypes.h"
#include "cp/cp-tree.h"


#include "gimple.h"
#include "gimple-iterator.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "tree-pass.h"
#include "context.h"
#include "classtype.h"
#include "functiondecl.h"


  const pass_data analyzer_pass_data = 
    {
      GIMPLE_PASS,
      "analyzer_pass",        /* name */
      OPTGROUP_NONE,          /* optinfo_flags */
      TV_NONE,                /* tv_id */
      PROP_gimple_any,        /* properties_required */
      0,                      /* properties_provided */
      0,                      /* properties_destroyed */
      0,                      /* todo_flags_start */
      0                       /* todo_flags_finish */
    };


inline string getReadLink( string fname ) {
  ifstream f;
  string readlink_str;

  string cmd;
  cmd = "readlink -e " + fname +  " > readlink_out.txt";
  int exit_code =   system(cmd.c_str());

  if ( exit_code != 0 )
    return "";

  f.open ("readlink_out.txt");
  assert ( f.good() );
  if ( !f.eof() )
    getline(f,readlink_str);
  f.close();
  exit_code +=  system("rm -f readlink_out.txt");

  if ( exit_code != 0  ) {
    stringstream msg;
    msg << "Cannot locate file "  << fname;
    throw runtime_error(msg.str());
  }

  return readlink_str;
}


class analyzer_pass : public gimple_opt_pass  {

 public:


  analyzer_pass(gcc::context *ctx , string _main_src_path );


  unsigned int execute(function* fun);
  void find_process_handles_in_func_defs( vector<VarDecl*> & non_member_vars );
  void print_class_types();
  void dump_xml( xmlwriter & xmlw );
  bool is_module_type(ClassType * n );
  bool is_base_name(string n );


 private:
  void add_base_name(string n );
  void add_class_type(ClassType * n );
  bool inherits_from(tree type , string base_qual_name );
  bool traverse_all_bases(ClassType * m,tree type, bool top );


  string main_src_path;
  vector<string>   module_base_names;
  vector<ClassType* >   class_types;
  vector<FunctionDecl*>  function_defs;
  vector<FunctionDecl*>  called_function_decls;

};

#endif
