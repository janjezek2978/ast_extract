/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "typename.h"



TypeName::TypeName ( tree & _type ): TypeBase(_type),tpl_value(NULL) {
  name = "typename";
  full_name = name;
  line = 0;
  col = 0;
  file = "<builtin>";
}

TypeBase::t_variant TypeName::get_variant() {
  return  TypeBase::V_TypeName;
}


void TypeName::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("TypeName");
  if ( tpl_value != NULL ) 
    tpl_value->dump_xml(xmlw);
  xmlw.closeElt();

}

void TypeName::set_tpl_value( TypeBase * v ) {
  tpl_value = v;
}
