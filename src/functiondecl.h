/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_FUNCTIONDECL_
#define _H_FUNCTIONDECL_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "tree-iterator.h"
#include "diagnostic.h"

#include "xmlwriter.h"
#include "scope.h"
#include "vardecl.h"
#include "expressionbase.h"
#include "loopexpression.h"
#include "callexpression.h"
#include "modifyexpression.h"
#include "declbase.h"


using namespace std;

class VarDecl;

class FunctionDecl : public DeclBase  {
 public:


  FunctionDecl ( tree & _decl );
  DeclBase::t_variant get_variant();

  void print();
  void dump_xml( xmlwriter & xmlw );
  void set_is_template(bool v );
  bool is_template();
  bool is_virtual();
  bool is_pure_virtual();
  bool is_main();
  bool is_constructor();
  bool is_static();
  bool is_member();
  bool is_overloaded_operator();
  bool is_declared_inline();
  bool is_method();
  bool has_body();
  bool is_process();
  void set_process_flag(bool v);

  unsigned int get_number_of_parms();
  vector<VarDecl*> & get_proc_handle_decls();
  void set_body_flag( bool v );
  void add_local_var_decl( VarDecl * decl );

 private:
  void analyzeBodyStatements( const tree & body );
  void analyzeExpr( tree  exp );


  bool template_flag;
  bool virtual_flag;
  bool pure_virtual_flag;
  bool main_flag;
  bool constructor_flag;
  bool static_flag;
  bool member_flag;
  bool overloaded_operator_flag;
  bool declared_inline_flag;
  bool method_flag;
  bool body_flag;
  bool process_flag;
  unsigned int number_of_parms;

  vector<VarDecl *> function_parms;
  vector<VarDecl *> local_vars;
  vector<ExpressionBase*> expressions;
  vector<VarDecl*>  proc_handle_decls;
  VarDecl * result;

  FunctionDecl();
};

#endif
