/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#include "vardecl.h"



VarDecl::VarDecl ( tree & _decl): DeclBase(_decl),t(NULL),tpl_value(NULL) {

  full_name = scope_str + "::" + name;

   tree type =  TREE_TYPE (decl);
  tree_code tc = TREE_CODE (type);

  t = NULL;
  switch ( tc ) {
  case BOOLEAN_TYPE:
  case INTEGER_TYPE:
  case REAL_TYPE:
  case VOID_TYPE: {
    t = new NativeCppType(type);
    break;
  }

  case POINTER_TYPE: {
    t = new PointerType(type);
    break;
  }


  case RECORD_TYPE:  {
    t = new ClassType(type,false);
    break;
  }

    // TO DO:  Debug the crash relted to array types

  case ARRAY_TYPE: {
    t = new ArrayType(decl,type);
    break;
  }

  default: {
    warning_at(DECL_SOURCE_LOCATION(decl),0,"Handling of type %s in variable declaration is not implemented.",get_tree_code_name(tc));
    break;

  }

  }


}

DeclBase::t_variant VarDecl::get_variant() {
  return V_VarDecl;
}


bool VarDecl::is_sc_process_handle() {
  if ( t ) {
    string type_name = t->get_name();
    if ( is_ClassType(t) && ( type_name.compare("sc_process_handle") == 0 ) ) {
      if ( name.length() > 7 ) {
	string suffix = name.substr(name.length()-7,name.length()-1);
	if ( suffix.compare("_handle") == 0 )
	  return true; 
      }
    }
  }
  return false;
}

TypeBase * VarDecl::get_type() { return t; }

void VarDecl::print( ) {
  inform ( DECL_SOURCE_LOCATION (decl),"%s Variable declaration at this location.",full_name.c_str());
}


void VarDecl::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("VarDecl");
  xmlw.attr("name",name).attr("full_name",full_name).attr("scope",scope_str);
  xmlw.openElt("location").attr("line",decl_line).attr("col",decl_col).attr("file",decl_file).closeElt();
  if ( t != NULL )
    t->dump_xml(xmlw);
  else {
    xmlw.openElt("UnknownType"); 
    xmlw.closeElt();
  }
  if ( tpl_value != NULL ) 
    tpl_value->dump_xml(xmlw);


  xmlw.closeElt();
}

void VarDecl::set_tpl_value( TemplateArgValue * v ) {

  tpl_value = v;
}

