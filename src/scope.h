/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_SCOPE_FUNCTION
#define _H_SCOPE_FUNCTION

#include <vector>
#include <string>


static std::string decl_scope (tree decl) {

  std::vector<std::string> scope_names;
  for (tree scope (CP_DECL_CONTEXT (decl));
       scope != global_namespace;
       scope = CP_DECL_CONTEXT (scope))
    {
      const char * name; 
      if (TREE_CODE (scope) == RECORD_TYPE)
  	scope = TYPE_NAME (scope);


      if ( TREE_CODE(scope) == NAMESPACE_DECL || TREE_CODE(scope) == TYPE_DECL ) {
	tree id (DECL_NAME (scope));
	name = IDENTIFIER_POINTER (id);
      } else
      	name = lang_hooks.decl_printable_name (scope, 0);

      scope_names.push_back(std::string(name));

    }

  std::string scope_str;
 
  for ( std::vector<std::string>::iterator i= scope_names.begin(); i != scope_names.end(); i++ ) {
    scope_str += "::" + *i;
  }

  if ( scope_str.length() == 0 )
    scope_str = "::";

  return scope_str;
}



#endif
