/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#ifndef XML_WRITER_H
# define XML_WRITER_H


# define HEADER "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

#include <deque>
#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>

using namespace std;


class xmlwriter
{
 public:

  xmlwriter( string _xml_file_name);
  ~xmlwriter();

  xmlwriter& openElt(string tag);
  xmlwriter& closeElt();
  xmlwriter& closeAll();
  xmlwriter& attr(string key, string val);
  xmlwriter& attr(string key, const int val);
  void close_file();
 private:
  ofstream xml_file;
  bool tag_open;
  bool new_line;
  string xml_file_name;
  std::deque<string> elt_stack;

   void closeTag();
   void indent();
   void write_escape(string str);
   void replaceAll(std::string& str, const std::string& from, const std::string& to);

};

#endif /* !XML_WRITER_H */
