/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "functiondecl.h"



FunctionDecl::FunctionDecl ( tree & _decl): 
  DeclBase(_decl),
  template_flag(false),
  virtual_flag(false),
  pure_virtual_flag(false),
  main_flag(false),
  constructor_flag(false),
  static_flag(false),
  member_flag(false),
  overloaded_operator_flag(false),
  declared_inline_flag(false),
  method_flag(false),
  body_flag(false),
  process_flag(false),
  number_of_parms(0),
  result(NULL) {

  full_name = lang_hooks.decl_printable_name (decl, 0x1);
  name = lang_hooks.decl_printable_name (decl,0);
  decl_line = DECL_SOURCE_LINE(decl);
  decl_col  = DECL_SOURCE_COLUMN(decl);
  decl_file = DECL_SOURCE_FILE (decl);
  scope_str =  decl_scope (decl);


  tree f_body = DECL_SAVED_TREE(decl);
  body_flag = ( f_body != NULL_TREE && f_body != error_mark_node );
 

  virtual_flag = DECL_VIRTUAL_P(decl);
  pure_virtual_flag = DECL_PURE_VIRTUAL_P(decl);
  main_flag =  DECL_MAIN_P (decl);
  constructor_flag =  DECL_CONSTRUCTOR_P (decl);
  static_flag =  DECL_STATIC_FUNCTION_P (decl);
  member_flag =   DECL_FUNCTION_MEMBER_P (decl);
  declared_inline_flag =  DECL_DECLARED_INLINE_P(decl) ;
  int tc;
  tc = TREE_CODE (TREE_TYPE (decl));
  method_flag = ( tc == METHOD_TYPE   );


  // tree current_function_parm = DECL_ARGUMENTS(decl );

  // if  ( current_function_parm  ) {
  //   tree parm;
  //   tree next;
  //   for (parm = current_function_parm; parm; parm = next) {
  //     next = DECL_CHAIN (parm);
  //     if (TREE_CODE (parm) == PARM_DECL) {
  // 	if (DECL_NAME (parm) == NULL_TREE || !VOID_TYPE_P (parm)) {
  // 	  function_parms.push_back( new VarDecl ( parm ) );
  // 	  number_of_parms++;
  // 	} else
  // 	  error ("parameter %qD declared void", parm);
  //     }

  //   }

  // }






  if ( body_flag ) {

    if ( !DECL_PURE_VIRTUAL_P(decl) ) {
      tree result_decl = DECL_RESULT (decl );
      result = new VarDecl(result_decl);
    }

    if ( EXPR_P(f_body)  ) {
      analyzeExpr(f_body);
    } else if (  TREE_CODE (f_body) == STATEMENT_LIST )  {
      analyzeBodyStatements(f_body);
    }


  }


}

DeclBase::t_variant FunctionDecl::get_variant() {
  return DeclBase::V_FuncDecl;
}

void FunctionDecl::print( ) {
  inform ( DECL_SOURCE_LOCATION (decl),"%s Function declaration at this location.",full_name.c_str());
}

void FunctionDecl::dump_xml( xmlwriter & xmlw ) {

  if ( has_body() ) {
    if ( is_method() ) {
      if (  is_declared_inline() ) 
  	xmlw.openElt("MethodDefininingDeclaration");
      else
  	xmlw.openElt("MethodDefinition");
    } else
      xmlw.openElt("FunctionDefinition");

  } else {
    if ( is_method() ) 
      xmlw.openElt("MethodDeclaration");
    else
      xmlw.openElt("FunctionDeclaration");
  }


  xmlw.attr("name",name); 
  xmlw.attr("full_name",full_name);
  xmlw.attr("scope",scope_str);
  xmlw.attr("template",is_template());
  xmlw.attr("virtual",is_virtual());
  xmlw.attr("pure_virtual",is_pure_virtual());
  xmlw.attr("main",is_main());
  xmlw.attr("sc_process",is_process());
  xmlw.attr("constructor",is_constructor());
  xmlw.attr("method",is_method());
  xmlw.attr("member",is_member());
  xmlw.attr("static",is_static());
  xmlw.attr("declared_inline",is_declared_inline());
  xmlw.attr("number_of_parms",get_number_of_parms());
  xmlw.openElt("location").attr("line",decl_line).attr("col",decl_col).attr("file",decl_file).closeElt();


  vector<ExpressionBase*> loop_expressions;
  vector<ExpressionBase*> call_expressions;
  vector<ExpressionBase*> mod_expressions;

    for ( vector<ExpressionBase *>::iterator i = expressions.begin(); i != expressions.end(); i++ ) {

      switch ( (*i)->get_variant() ) {
      case ExpressionBase::V_CallExpression: {
  	call_expressions.push_back(*i);
  	break;
      }
      // case ExpressionBase::V_LoopExpression: {
      // 	loop_expressions.push_back(*i);
      // 	break;
      // }
      // case ExpressionBase::V_ModifyExpression: {
      // 	mod_expressions.push_back(*i);
      // 	break;
      // }
      default: ;
      }

    }


  if ( function_parms.size() ) {
    xmlw.openElt("parmeteres");
    for ( vector<VarDecl *>::iterator i = function_parms.begin(); i != function_parms.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  if ( local_vars.size() ) {
    xmlw.openElt("local_variables");
    for ( vector<VarDecl *>::iterator i = local_vars.begin(); i != local_vars.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  if ( loop_expressions.size() ) {
    xmlw.openElt("loop_expressions");
    for ( vector<ExpressionBase *>::iterator i = loop_expressions.begin(); i != loop_expressions.end(); i++ )
      (*i)->dump_xml(xmlw);
    xmlw.closeElt();
  }

  if ( call_expressions.size() ) {
    xmlw.openElt("call_expressions");
    for ( vector<ExpressionBase *>::iterator i = call_expressions.begin(); i != call_expressions.end(); i++ )
      (*i)->dump_xml(xmlw);
    xmlw.closeElt();
  }

  if ( mod_expressions.size() ) {
    xmlw.openElt("modify_expressions");
    for ( vector<ExpressionBase *>::iterator i = mod_expressions.begin(); i != mod_expressions.end(); i++ )
      (*i)->dump_xml(xmlw);
    xmlw.closeElt();
  }


  if ( result ) {
    xmlw.openElt("result");
    result->dump_xml(xmlw);
    xmlw.closeElt();
  }

  xmlw.closeElt();
}

void FunctionDecl::analyzeBodyStatements( const tree & t ) {
  if ( tsi_end_p(tsi_start(t))  ) {
    return;
  } else
    for (tree_stmt_iterator it = tsi_start(t); !tsi_end_p(it); tsi_next(&it)) {
      tree stmt = tsi_stmt(it);
      int tc = TREE_CODE (stmt);
      if (  tc == STATEMENT_LIST )  {
        analyzeBodyStatements(stmt);
      } else
        analyzeExpr(STRIP_NOPS(stmt));
    }


}



void FunctionDecl::analyzeExpr(  tree exp ) {
  if ( !exp )
    return;

  tree_code tc = TREE_CODE(exp);

  if ( CAN_HAVE_LOCATION_P(exp) && 0  ) {
    cout <<  EXPR_FILENAME(exp);   
    cout << ":" << LOCATION_LINE (EXPR_CHECK (exp)->exp.locus);
    cout << ":" <<  LOCATION_COLUMN (EXPR_CHECK (exp)->exp.locus) << " " <<  get_tree_code_name(tc)  <<endl;
  }



  switch( tc ) {

  case ERROR_MARK: {
    gcc_unreachable();
    break;
  }


  case TREE_LIST:
    while (exp && exp != error_mark_node)
      {
	if (TREE_PURPOSE (exp))
	  {
	    analyzeExpr( TREE_PURPOSE (exp));
	  }
	analyzeExpr( TREE_VALUE (exp));
	exp = TREE_CHAIN (exp);
      }
    break;



  case TREE_VEC: {
    {
      size_t i;
      if (TREE_VEC_LENGTH (exp) > 0)
	{
	  size_t len = TREE_VEC_LENGTH (exp);
	  for (i = 0; i < len - 1; i++)
	    {
	      analyzeExpr( TREE_VEC_ELT (exp, i));
	    }
	  analyzeExpr( TREE_VEC_ELT (exp, len - 1));
	}
    }
    break;
  }



  case STATEMENT_LIST: {
    analyzeBodyStatements(exp);
    break;
  }

  case BIND_EXPR: {
    if (BIND_EXPR_VARS (exp))
      {

	for (tree local_decl = BIND_EXPR_VARS (exp); local_decl; local_decl = DECL_CHAIN (local_decl)) {
          VarDecl * v =   new VarDecl ( local_decl );
	  local_vars.push_back( v );
	  if ( v->is_sc_process_handle() )
	    proc_handle_decls.push_back(v);
	}
      }
    tree body = BIND_EXPR_BODY (exp);
    analyzeExpr(body );
    break;
  }


  case DECL_EXPR:
  case NOP_EXPR:
  case INDIRECT_REF:
  case CONVERT_EXPR: 
  case ARROW_EXPR:
  case ADDR_EXPR:
  case EXPR_STMT:
  case CLEANUP_POINT_EXPR:
  case TRY_CATCH_EXPR:
  case TRY_FINALLY_EXPR: {
    analyzeExpr(TREE_OPERAND(exp, 0));
    break;
  }


  case CALL_EXPR: {
    expressions.push_back( new CallExpression(exp) );
    break;
  }

  // case LOOP_EXPR: {
  //   expressions.push_back( new LoopExpression(exp) );
  //   analyzeExpr(LOOP_EXPR_BODY(exp));
  //   break;
  // }

  // case MODIFY_EXPR: {
  //   expressions.push_back( new ModifyExpression(exp) );
  //   break;
  // }

  default: {
    ;
  }

  }

}




void FunctionDecl::set_is_template(bool v ) { template_flag=v; }
bool FunctionDecl::is_template() { return template_flag; }

  bool FunctionDecl::is_virtual(){ return virtual_flag; }
  bool FunctionDecl::is_pure_virtual(){ return pure_virtual_flag; }
  bool FunctionDecl::is_main(){ return main_flag; }
  bool FunctionDecl::is_constructor(){ return constructor_flag; }
  bool FunctionDecl::is_static(){ return static_flag; }
  bool FunctionDecl::is_member(){ return member_flag; }
  bool FunctionDecl::is_overloaded_operator(){ return overloaded_operator_flag; }
  bool FunctionDecl::is_declared_inline(){ return declared_inline_flag; }
  bool FunctionDecl::is_method(){ return method_flag; }
  bool FunctionDecl::has_body(){ return body_flag; }
  bool FunctionDecl::is_process() { return process_flag; }
  void FunctionDecl::set_process_flag( bool v) { process_flag = v; }
  unsigned int FunctionDecl::get_number_of_parms() { return number_of_parms; }
  vector<VarDecl*> &  FunctionDecl::get_proc_handle_decls() { return proc_handle_decls; }
void FunctionDecl::set_body_flag( bool v ) { body_flag = v; }
void FunctionDecl::add_local_var_decl( VarDecl * decl ) { local_vars.push_back(decl); } 

