/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include <iostream>
#include "gcc-plugin.h"
#include "plugin-version.h"
#include "analyzer_pass.h"
#include "xmlwriter.h"
#include "functiondecl.h"
#include "vardecl.h"




using namespace std;



int plugin_is_GPL_compatible;

static struct plugin_info ast_extract_info = { "1.1", "Extract AST data from SystemC." };

namespace {
  struct register_pass_info pass_info;
}

string xml_file_name;
vector<string> included_files;
string main_src_path;
string decl_loc_file;

vector<VarDecl*>  non_member_variables;

 
static void callback_finish(void *gcc_data, void *user_data) {
  analyzer_pass * mp = static_cast<analyzer_pass*>(pass_info.pass);
  xmlwriter writer(xml_file_name);
  assert(mp);
  writer.openElt("file").attr("name",main_input_filename);
  mp->dump_xml(writer);

  if ( non_member_variables.size()  ) { 
    writer.openElt("NonMemberVariables");
    for ( auto vd : non_member_variables ) {
      vd->dump_xml(writer);
    }
    writer.closeElt();
  }

  if ( included_files.size()  ) {
    writer.openElt("IncludedFiles");
    for ( auto f : included_files ) {
      writer.openElt("File");
      writer.attr("name",f);
      writer.closeElt();
    }
    writer.closeElt();
  }

  writer.closeElt();
  writer.close_file();
  fnotice (stderr, "Output written to %s\n",xml_file_name.c_str());
}

static void callback_end_decl(void *gcc_data, void *user_data) {
  tree decl = (tree) gcc_data;
  if ( decl && ! (( decl  == error_mark_node) || DECL_IS_BUILTIN (decl) ) ) {
    tree_code tc = TREE_CODE(decl);
    string loc_file  = DECL_SOURCE_FILE (decl);
    string current_filname = loc_file;

    if ( decl_loc_file.compare(loc_file) != 0 )  {
      current_filname = getReadLink(loc_file);
      decl_loc_file = loc_file;
    }

    if ( ( tc == VAR_DECL ) &&  (current_filname.find( main_src_path ) == 0 ) || (main_src_path.length() == 0 )  ) {
	VarDecl * vd = new VarDecl(decl);
	if ( vd->get_type() ) { // Not all types are supported
	  string type_n;
	  if ( is_ArrayType(vd->get_type()) ) { 
	    ArrayType * a = is_ArrayType(vd->get_type());
	    TypeBase * elt_type = a->get_element_type();
	    if ( elt_type )
	      type_n = elt_type->get_name();
	    else
	      type_n = "_UnknownType";
	  } else
	    type_n = vd->get_type()->get_name();
	  string scope = vd->get_scope();
	  if ( ( type_n.find("__") != 0 ) && ( scope.compare("::") !=0 ) )  //  Names starting with "__" are reserved. Global variables are excluded.
	    non_member_variables.push_back(vd);
	}
    }
  }

}

static void callback_include  (void *gcc_data, void *user_data) {
  string incl_file((char*) gcc_data);

  if ( incl_file.length() && ( incl_file.compare(main_input_filename) != 0 ) && !( (  incl_file.find('<') != string::npos ) || ( incl_file.find('>') != string::npos )   ) ) {
    incl_file = getReadLink(incl_file);
    if ( (incl_file.find( main_src_path ) == 0 ) || (main_src_path.length() == 0 ) )
      included_files.push_back(incl_file);
  }

}

int plugin_init (struct plugin_name_args *plugin_info,struct plugin_gcc_version *version) {


  if ( !plugin_default_version_check (version, &gcc_version)) {
    error("This GCC plugin is for version %d.%d",GCCPLUGIN_VERSION_MAJOR,GCCPLUGIN_VERSION_MINOR);
    return 1;
  }


  bool default_output = true;

  for (int i = 0; i < plugin_info->argc; i++) { 

    if ( !strcmp(plugin_info->argv[i].key,"src_path") ) {
      main_src_path =  string(plugin_info->argv[i].value);
      fnotice (stderr,"Main source path: %s\n",main_src_path.c_str());
    } 

    if ( !strcmp(plugin_info->argv[i].key,"output") ) {
      xml_file_name =  plugin_info->argv[i].value;
      default_output = false;
    }

  }

  if ( ! main_src_path.length() ) 
    warning(0,"Main source path has not been set.");

  if ( default_output ) 
    xml_file_name = string(main_input_filename) + ".xml";


  fnotice (stderr, "Output will be written to %s\n",xml_file_name.c_str());


  register_callback(plugin_info->base_name,PLUGIN_INFO,NULL,&ast_extract_info);
  pass_info.pass = new analyzer_pass(g,main_src_path);
  pass_info.reference_pass_name = "cfg";
  pass_info.ref_pass_instance_number = 1;
  pass_info.pos_op = PASS_POS_INSERT_BEFORE;

  register_callback (plugin_info->base_name, PLUGIN_PASS_MANAGER_SETUP, NULL, &pass_info);
  register_callback(plugin_info->base_name,PLUGIN_FINISH,callback_finish, NULL);
  register_callback(plugin_info->base_name,PLUGIN_FINISH_DECL,callback_end_decl, NULL);
  register_callback(plugin_info->base_name,PLUGIN_INCLUDE_FILE,callback_include, NULL);
  return seen_error();
}
