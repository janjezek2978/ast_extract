/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "classtype.h"



ClassType::ClassType ( tree & _type,bool analyze_memebers_en ):  TypeBase(_type) ,module(false),module_base(false) {
  decl = TREE_CHAIN(type);
  full_name = lang_hooks.decl_printable_name (type, 0x2);
  name = lang_hooks.decl_printable_name (decl,0);
  scope = decl_scope(decl);

  if (  decl == error_mark_node || ! decl || (analyze_memebers_en && !COMPLETE_TYPE_P (type)) ) {
    warning_at (EXPR_LOCATION (type),0, "Incomplete type %q+D ", type);
    fatal_error(EXPR_LOCATION (type),"Failed to find definition.");
  }
  else if ( name.find("__") != 0  )
    inform ( DECL_SOURCE_LOCATION (decl),"Class type definition %s",full_name.c_str());

  line = DECL_SOURCE_LINE(decl);
  col  = DECL_SOURCE_COLUMN(decl);
  file = DECL_SOURCE_FILE (decl);


  tree decl_type = TREE_TYPE(decl);
  template_flag = ( ( full_name.find("<") != string::npos ) && (full_name.find(">") != string::npos) );
  ti_arg_count = 0;
  if ( template_flag ) {

    tree t_info = CLASSTYPE_TEMPLATE_INFO (decl_type);
    tree tpl = TI_TEMPLATE (t_info);
    tree args =   TI_ARGS(t_info);
    tree parms = DECL_TEMPLATE_PARMS (tpl);
    int parm_count;

    parms = TREE_CODE (parms) == TREE_LIST ? TREE_VALUE (parms) : NULL_TREE;
    parm_count = parms ? TREE_VEC_LENGTH (parms) : 0;


    args = INNERMOST_TEMPLATE_ARGS (args);
    assert( TREE_VEC_LENGTH (args) == parm_count );
    for ( int ix = 0; ix !=  TREE_VEC_LENGTH (args); ix++) {
      tree elt = TREE_VEC_ELT (args, ix);
      TemplateArgValue * arg_val = new TemplateArgValue(elt);
      // Collect template parms here
      tree parm;
      parm = TREE_VALUE (TREE_VEC_ELT (parms, ix));
      tree_code tc = TREE_CODE (parm);

      if ( tc == PARM_DECL ) {
        VarDecl * vd = new VarDecl(parm);
	vd->set_tpl_value(arg_val);
	template_parm_decls.push_back(vd);
      } 

      if ( tc == TYPE_DECL )  {
	TypeDecl * td = new TypeDecl(parm);
	td->set_tpl_value(arg_val->get_value_as_type());
	template_parm_decls.push_back(td);
      }

      template_arg_values.push_back(arg_val);
    }



  } 

  instance_name = name;    
  for ( vector<TemplateArgValue*>::iterator i = template_arg_values.begin(); i != template_arg_values.end(); i++ ) {
    instance_name += "_" + (*i)->get_value_as_string();
  }

  if  ( analyze_memebers_en )
    analyze_memebers(decl);

}

TypeBase::t_variant ClassType::get_variant() {
  return  TypeBase::V_ClassType;
}


string ClassType::get_instance_name() {
  return instance_name;
}


void ClassType::print() {
  cout << name << " " << instance_name << " " << full_name  << endl;
}

void ClassType::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("ClassType");
  xmlw.attr("scope",scope).attr("name",name).attr("full_name",get_full_name()).attr("instance_name",get_instance_name()).attr("module",is_module()).attr("module_base",is_module_base());
  xmlw.openElt("definition_location").attr("line",get_line()).attr("col",get_col()).attr("file",get_file()).closeElt();
  if (  direct_bases.size()  ) {
    xmlw.openElt("direct_bases");
    for ( vector<ClassType *>::iterator i = direct_bases.begin(); i != direct_bases.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  assert( template_parm_decls.size() == template_parm_decls.size() );

  if ( template_parm_decls.size() ) {
    xmlw.openElt("template_parms");
    for ( vector<DeclBase*>::iterator i = template_parm_decls.begin(); i != template_parm_decls.end(); i++ ) {
      if ( is_VarDecl(*i) ) 
	is_VarDecl(*i)->dump_xml(xmlw);

      if ( is_TypeDecl(*i) ) 
	is_TypeDecl(*i)->dump_xml(xmlw);

    }
    xmlw.closeElt();


  }

  if ( member_var_decls.size() ) {
    xmlw.openElt("member_variables");
    for ( vector<VarDecl *>::iterator i = member_var_decls.begin(); i != member_var_decls.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  if ( member_type_decls.size() ) {
    xmlw.openElt("member_typedefs");
    for ( vector<TypeDecl *>::iterator i = member_type_decls.begin(); i != member_type_decls.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }


  if ( member_function_decls.size() ) {
    xmlw.openElt("member_functions");
    for ( vector<FunctionDecl *>::iterator i = member_function_decls.begin(); i != member_function_decls.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  if ( inherits_from.size() ) {
    xmlw.openElt("inherits_from");
    for ( vector<ClassType *>::iterator i = inherits_from.begin(); i != inherits_from.end(); i++ ) {
      (*i)->dump_xml(xmlw);
    }
    xmlw.closeElt();
  }

  xmlw.closeElt();

}

bool ClassType::is_template() {
  return template_flag;
}

bool  ClassType::is_module(){ 
  return module; 
}

bool  ClassType::is_module_base() { 
  return module_base;
}


void  ClassType::set_is_module( bool v ){ 
  module = v ; 
}

void  ClassType::set_is_module_base( bool v  ) { 
  module_base = v;
}



bool ClassType::is_direct_base_name(const string &n ) {
  for ( vector<ClassType *>::iterator i = direct_bases.begin(); i != direct_bases.end(); i++ ) {
    if ( n.compare((*i)->get_name()) == 0 )
      return true;
  } 
  return false;
}


void ClassType::add_direct_base(ClassType * b ) {
    direct_bases.push_back(b);
}




bool ClassType::is_inherits_from(const string &n ) {
  for ( vector<ClassType *>::iterator i = inherits_from.begin(); i != inherits_from.end(); i++ ) {
    if ( n.compare((*i)->get_name()) == 0 )
      return true;
  } 
  return false;
}


void ClassType::add_inherits_from(ClassType * c ) {
    inherits_from.push_back(c);
}




void ClassType::analyze_memebers( tree decl ) {

    vector<tree> member_decls;

    for (tree d (TYPE_FIELDS (type)); d != 0; d = TREE_CHAIN (d)) {
      if ( DECL_ARTIFICIAL (d) )
	continue;
      member_decls.push_back(d);
    }

    for (tree d (TYPE_METHODS (type)); d != 0; d = TREE_CHAIN (d)) {
      if ( DECL_ARTIFICIAL (d) )
	continue;
      member_decls.push_back(d);
   }


    for (vector<tree>::iterator i = member_decls.begin(); i != member_decls.end(); i++ ) {
      tree_code member_dc (TREE_CODE (*i));

      switch ( member_dc ) {

      case FUNCTION_DECL: {
      	FunctionDecl * fd = new FunctionDecl(*i);
      	member_function_decls.push_back(fd);	
      	break;
      }

      case TEMPLATE_DECL: {

      	tree fn = DECL_TEMPLATE_RESULT(*i);
      	FunctionDecl * fd = new FunctionDecl(fn);
      	fd->set_is_template(true);
	member_function_decls.push_back(fd);	
      	break;
      }

      case TYPE_DECL: {
	TypeDecl * td = new TypeDecl(*i);	
	member_type_decls.push_back(td);
	break;
      }


      case CONST_DECL:
      case FIELD_DECL:
      case VAR_DECL: {
      	VarDecl * vd = new VarDecl(*i);
      	member_var_decls.push_back(vd);	
      	break;
      }

      default: {
	warning_at(DECL_SOURCE_LOCATION(*i),0,"Handling members of type %s is not implemented.",get_tree_code_name(member_dc));
      }

      }

    }



}


vector<FunctionDecl *> & ClassType::get_member_function_decls() {
  return member_function_decls;
}


