/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "analyzer_pass.h"


analyzer_pass::analyzer_pass(gcc::context *ctx , string _main_src_path )
   : gimple_opt_pass(analyzer_pass_data, ctx),
    main_src_path(_main_src_path)
    {

    }


unsigned int analyzer_pass::execute(function* fun)  {
  gimple_seq gimple_body = fun->gimple_body;
  gimple_stmt_iterator i;

  if ( gimple_body ) {
    if ( fun->decl && !DECL_DECLARED_INLINE_P(fun->decl) && ! DECL_MAIN_P (fun->decl)   ) {

      tree decl   = fun->decl;
      FunctionDecl * fdef = new FunctionDecl(decl);
      fdef->set_body_flag(true);

      string loc_file = getReadLink(fdef->get_decl_file());
      string func_name = fdef->get_name();
      if ( ( (loc_file.find( main_src_path ) == 0 ) || (main_src_path.length() == 0 ) ) && !( ( func_name.find("~") == 0 ) || ( func_name.find("__") == 0 ) ||  (func_name.find("_GLOBAL_") == 0 )  ) ) {

	function_defs.push_back(fdef);
      }
    }
  }


  for (i = gsi_start (gimple_body); !gsi_end_p (i); gsi_next (&i)) {
    gimple gs = gsi_stmt (i);


    if (!gs)
      continue;

    if ( gimple_code (gs) == GIMPLE_CALL && !  gimple_call_builtin_p (gs) ) {
      gcall * call_gs = as_a <gcall *> (gs);
      tree call = gimple_call_fn (call_gs);
      tree fn = call;

      while ( (TREE_CODE (fn) == NOP_EXPR) ||
	      (TREE_CODE (fn) == ADDR_EXPR ) ||
	      (TREE_CODE (fn) == INDIRECT_REF )  ) {
	fn = TREE_OPERAND (fn, 0);
      }


      if ( TREE_CODE (fn) == FUNCTION_DECL ) {
	tree_code tc = TREE_CODE (TREE_TYPE (fn));
	if ( DECL_NAME (fn) && DECL_CLASS_SCOPE_P (fn) && ( tc == METHOD_TYPE   )  )  {
	  FunctionDecl * fdecl = new FunctionDecl(fn);
	  string loc_file = getReadLink(fdecl->get_decl_file());
	  string func_name = fdecl->get_name();
	  if ( ( (loc_file.find( main_src_path ) == 0 ) || (main_src_path.length() == 0 ) ) && !( ( func_name.find("~") == 0 ) ||  ( func_name.find("__") == 0 ) ||  (func_name.find("_GLOBAL_") == 0 )  ) )
	    called_function_decls.push_back(fdecl);

	
	  if (  DECL_CONSTRUCTOR_P (fn)   )  {
	    tree class_type = DECL_CONTEXT (fn);

	    string class_name = lang_hooks.decl_printable_name (class_type, 0x2);
	    string loc_file = DECL_SOURCE_FILE (TREE_CHAIN(class_type));
	    loc_file = getReadLink(loc_file);
	    if ( ( loc_file.length() ) &&  ( (loc_file.find( main_src_path ) == 0 ) || (main_src_path.length() == 0 ) ) &&   !is_base_name(class_name)   ) {
	      ClassType * ct = new ClassType(class_type,true);
	      ct->set_is_module( inherits_from(class_type,"sc_core::sc_module")  );
	      add_class_type(ct);
	      traverse_all_bases(ct,class_type,true);
	    } 
	  
	  }
	}
      }
    }
  }

  return 0;
}

void analyzer_pass::find_process_handles_in_func_defs(vector<VarDecl*> & non_member_vars ) {



  vector<VarDecl*>  proc_handle_decls;

  for ( auto f : function_defs ) {
    for ( auto d : f->get_proc_handle_decls() ) {
      proc_handle_decls.push_back(d);
    }
  }

    for ( auto vd : non_member_vars ) {
      if ( vd->is_sc_process_handle() ) 
	proc_handle_decls.push_back(vd);
    }


    for ( auto vd : proc_handle_decls ) {
      string var_name = vd->get_name();
      string decl_scope = vd->get_scope();
      string func_name = var_name.substr(0,var_name.find("_handle"));
      for ( auto f : function_defs  ) {

      	if ( ( func_name.compare( f->get_name() ) == 0 ) && ( decl_scope.find(f->get_scope()+"::") == 0  )  ) {
      	  f->set_process_flag(true);
      	  break;
      	}
      }

      for ( auto c : class_types ) {
	for ( auto f : c->get_member_function_decls()  ) {
	  if ( ( func_name.compare( f->get_name() ) == 0 ) && ( decl_scope.find(f->get_scope()+"::") == 0  )  ) {
	    f->set_process_flag(true);
	    break;
	  }
	}

      }

    }

}



void analyzer_pass::print_class_types() {
  for ( vector<ClassType*>::iterator i = class_types.begin(); i != class_types.end(); i++ ) 
    (*i)->print();
}

void analyzer_pass::dump_xml( xmlwriter & xmlw ) {
  for ( vector<ClassType*>::iterator i = class_types.begin(); i != class_types.end(); i++ ) 
    (*i)->dump_xml(xmlw);

  for ( vector<FunctionDecl*>::iterator i = function_defs.begin(); i != function_defs.end(); i++ ) 
    (*i)->dump_xml(xmlw);
  
  if ( called_function_decls.size() ) {
    xmlw.openElt("CalledFunctions");
    for ( vector<FunctionDecl*>::iterator i = called_function_decls.begin(); i != called_function_decls.end(); i++ ) 
      (*i)->dump_xml(xmlw);
    xmlw.closeElt();
  }

}


bool analyzer_pass::is_module_type(ClassType * n ) {
  for ( vector<ClassType*>::iterator i = class_types.begin(); i != class_types.end(); i++ ) {
    if ( n->get_full_name().compare((*i)->get_full_name()) == 0 )
      return true;
  } 
  return false;
}

void analyzer_pass::add_class_type(ClassType * n ) {
  if ( ! is_module_type(n) ) 
    class_types.push_back(n);
}


bool analyzer_pass::is_base_name(string n ) {
  for ( vector<string>::iterator i = module_base_names.begin(); i != module_base_names.end(); i++ ) {
    if ( n.compare(*i) == 0 )
      return true;
  } 
  return false;
}

void analyzer_pass::add_base_name(string n ) {
  if ( !is_base_name(n) )
    module_base_names.push_back(n);
}


bool analyzer_pass::inherits_from(tree type , string base_qual_name ) {

  if (!COMPLETE_TYPE_P (type))
    return false;

  tree biv (TYPE_BINFO (type));
  size_t n (biv ? BINFO_N_BASE_BINFOS (biv) : 0);

  if ( n == 0 )
    return false; 

  for (size_t i (0); i < n; i++) {
    tree bi (BINFO_BASE_BINFO (biv, i));
    tree b_type (TYPE_MAIN_VARIANT (BINFO_TYPE (bi)));
    tree b_decl (TYPE_NAME (b_type));
    string base_name = lang_hooks.decl_printable_name (b_decl, 2);
    if ( base_qual_name.compare(base_name) == 0 ) {
      return true;
    } else
      return inherits_from(b_type,base_qual_name);
  }

  return false;
}


bool analyzer_pass::traverse_all_bases(ClassType * m, tree type, bool top ) {
  tree biv (TYPE_BINFO (type));
  size_t n (biv ? BINFO_N_BASE_BINFOS (biv) : 0);
  if ( n == 0 )
    return false; 

  for (size_t i (0); i < n; i++) {
    tree bi (BINFO_BASE_BINFO (biv, i));
    tree b_type (TYPE_MAIN_VARIANT (BINFO_TYPE (bi)));
    tree b_decl (TYPE_NAME (b_type));
    string base_name = lang_hooks.decl_printable_name (b_decl, 2);

    ClassType * base = new ClassType(b_type,true);
    base->set_is_module_base(true);
    if  ( base->get_scope().compare("::sc_core") != 0 ) 
    traverse_all_bases(base,b_type,true);
    if ( top ) 
      m->add_direct_base(base);
    m->add_inherits_from(base);
    add_base_name(base_name);

    if  ( base->get_scope().compare("::sc_core") != 0 ) 
    traverse_all_bases(m,b_type,false);
  }

  return true;
}



