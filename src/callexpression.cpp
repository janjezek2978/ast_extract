/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "callexpression.h"



CallExpression::CallExpression ( tree & _exp ): ExpressionBase(_exp),wait_call_flag(false),analyzed(false) {

  tree fn_decl = tree_strip_nop_conversions (CALL_EXPR_FN (exp));
 
  while ( fn_decl != NULL_TREE && TREE_CODE(fn_decl ) != ERROR_MARK && ( TREE_CODE(fn_decl ) == ADDR_EXPR || TREE_CODE(fn_decl ) == OBJ_TYPE_REF || TREE_CODE(fn_decl ) ==  INDIRECT_REF || TREE_CODE(fn_decl ) == POINTER_PLUS_EXPR ||  TREE_CODE(fn_decl ) == COMPONENT_REF || TREE_CODE(fn_decl ) == NOP_EXPR || TREE_CODE(fn_decl ) == OVERLOAD ||  TREE_CODE(fn_decl ) == BASELINK  ) ) {



    if ( TREE_CODE(fn_decl ) == COMPONENT_REF )
      fn_decl = TREE_OPERAND(fn_decl, 1);
    else if ( TREE_CODE(fn_decl ) == OVERLOAD ) {
      fn_decl = get_first_fn(fn_decl);
    } else if (  TREE_CODE(fn_decl ) == BASELINK  ) {
      fn_decl = BASELINK_FUNCTIONS (CONST_CAST_TREE (fn_decl) );
    } else
      fn_decl = TREE_OPERAND(fn_decl, 0);
    


  }

  if ( TREE_CODE(fn_decl ) == FUNCTION_DECL ) {
    fn_full_name = lang_hooks.decl_printable_name (fn_decl, 0x1);
    fn_name = lang_hooks.decl_printable_name (fn_decl,0);
    fn_decl_line = DECL_SOURCE_LINE(fn_decl);
    fn_decl_col  = DECL_SOURCE_COLUMN(fn_decl);
    fn_decl_file = DECL_SOURCE_FILE (fn_decl);
    wait_call_flag =  ( fn_full_name.compare("sc_core::sc_module::wait") == 0  )  || ( fn_full_name.compare("sc_core::wait") == 0 ) ;
    analyzed = true;
  }

}

ExpressionBase::t_variant CallExpression::get_variant() {
  return  ExpressionBase::V_CallExpression;
}


void CallExpression::dump_xml( xmlwriter & xmlw ) {
  if ( ! analyzed )
    return;
  xmlw.openElt("CallExpression");
  xmlw.attr("callee_name",fn_name);
  xmlw.attr("callee_full_name",fn_full_name);
  xmlw.attr("callee_decl_line",fn_decl_line);
  xmlw.attr("callee_decl_col",fn_decl_col);
  xmlw.attr("callee_decl_file",fn_decl_file);
  xmlw.attr("wait_call",wait_call_flag);
  xmlw.closeElt();

}

bool CallExpression::is_wait_call() { return wait_call_flag; }
