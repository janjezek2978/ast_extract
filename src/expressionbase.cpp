/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "expressionbase.h"



ExpressionBase::ExpressionBase ( tree & _exp ): exp(_exp) {

  if ( CAN_HAVE_LOCATION_P(exp)  ) {
    line =  LOCATION_LINE (EXPR_CHECK (exp)->exp.locus);;
    col = LOCATION_COLUMN (EXPR_CHECK (exp)->exp.locus);
    file = EXPR_FILENAME(exp);
  } else {
    line = 0;
    col = 0;
    file = "<builtin>";
  }

}


string  ExpressionBase::get_scope() {
  return scope;
}


void ExpressionBase::print() {
  cout << "expr " << file << ":" << line  << ":" << col << endl;
}


unsigned int ExpressionBase::get_line(){ return line; }

unsigned int ExpressionBase::get_col(){ return col; }

string  ExpressionBase::get_file(){ return file; }

