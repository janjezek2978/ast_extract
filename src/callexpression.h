/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_CALLEXPRESSION_
#define _H_CALLEXPRESSION_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "expressionbase.h"


using namespace std;

class CallExpression: public ExpressionBase {
public:


CallExpression ( tree & _type );
ExpressionBase::t_variant get_variant();
void dump_xml( xmlwriter & xmlw );

 bool is_wait_call();

private:

  string fn_name;
  string fn_full_name;
  unsigned int fn_decl_line;
  unsigned int fn_decl_col;
  string fn_decl_file;
  bool wait_call_flag;
  bool analyzed;

CallExpression();
};

#endif
