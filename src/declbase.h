/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#ifndef _H_DECLBASE_
#define _H_DECLBASE_

#include <iostream>

#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"


#include "scope.h"

using namespace std;

class DeclBase {
 public:


  enum t_variant {
    V_Undef = 0,
    V_TypeDecl,
    V_VarDecl,
    V_FuncDecl
  };



  DeclBase ( tree & _decl );
  virtual t_variant get_variant() = 0;
  virtual void print();
  string get_full_name();
  string get_name();
  unsigned int get_decl_line();
  unsigned int get_decl_col();
  string get_decl_file();
  string get_scope();

 protected:
  tree & decl;
  string name;
  string full_name;
  string scope_str;
  unsigned int decl_line;
  unsigned int decl_col;
  string decl_file;
 private:
  DeclBase();
};

#endif
