/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#ifndef _H_VARDECL_
#define _H_VARDECL_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"

#include "xmlwriter.h"
#include "scope.h"
#include "typebase.h"
#include "nativecpptype.h"
#include "classtype.h"
#include "pointertype.h"
#include "arraytype.h"
#include "declbase.h"
#include "templateargvalue.h"

class TemplateArgValue;
using namespace std;

class VarDecl : public DeclBase {
 public:

  VarDecl ( tree & _decl );
  bool is_sc_process_handle();
  t_variant get_variant();
  TypeBase * get_type();
  void print();
  void dump_xml( xmlwriter & xmlw );
  void set_tpl_value( TemplateArgValue * v );
 private:
  TypeBase * t;
  TemplateArgValue * tpl_value;
  VarDecl();

};




inline VarDecl * is_VarDecl(DeclBase * n) {
  VarDecl * rtn;
  if (  (n->get_variant() == DeclBase::V_VarDecl) && ( n != NULL) )
    rtn = static_cast<VarDecl*>(n);
  else
    rtn = NULL;
  return rtn;
}


#endif
