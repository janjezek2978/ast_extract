/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_EXPRESSIONBASE_
#define _H_EXPRESSIONBASE_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"


using namespace std;

class ExpressionBase {
 public:

  enum t_variant {
    V_Undef = 0,
    V_CallExpression,
    V_LoopExpression,
    V_ModifyExpression
  };


  ExpressionBase ( tree & _expression );
  virtual t_variant get_variant() = 0;
  virtual void print();
  virtual void dump_xml( xmlwriter & xmlw ) = 0;


  string  get_scope();
  unsigned int get_line();
  unsigned int get_col();
  string  get_file();


 protected:
  tree & exp;
  string  scope;
  unsigned int line;
  unsigned int col;
  string  file;

 private:
  ExpressionBase();
};

#endif
