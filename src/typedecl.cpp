/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#include "typedecl.h"



TypeDecl::TypeDecl ( tree & _decl): DeclBase(_decl),t(NULL) {


  
  full_name =  lang_hooks.decl_printable_name (decl,1);

  tree type = TREE_TYPE(decl);
  tree_code tc =  TREE_CODE (type);


  switch ( tc ) {
  case BOOLEAN_TYPE:
  case INTEGER_TYPE:
  case REAL_TYPE:
  case VOID_TYPE: {
    t = new NativeCppType(type);
    break;
  }

  case POINTER_TYPE: {
    t = new PointerType(type);
    break;
  }

  case TEMPLATE_TYPE_PARM: {
    t = new TypeName(type);
    break;
  }


  case RECORD_TYPE:  {
    t = new ClassType(type,false);
    break;
  }


  default: {
    warning_at(DECL_SOURCE_LOCATION(decl),0,"Handling of type %s in typedef declaration is not implemented.",get_tree_code_name(tc));
    break;
  }

  }


}




DeclBase::t_variant TypeDecl::get_variant() {
  return V_TypeDecl;
}

void TypeDecl::print() {
  cout << name <<  endl;
}

void TypeDecl::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("TypeDecl");
  xmlw.attr("name",name).attr("full_name",full_name).attr("scope",scope_str);
  xmlw.openElt("location").attr("line",decl_line).attr("col",decl_col).attr("file",decl_file).closeElt();
  if ( t != NULL )
    t->dump_xml(xmlw);

  xmlw.closeElt();
}

void TypeDecl::set_tpl_value( TypeBase * v ) {
  if  ( is_TypeName(t) )
    is_TypeName(t)->set_tpl_value(v);
}

