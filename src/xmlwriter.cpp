/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#include "xmlwriter.h"

xmlwriter::xmlwriter( string _xml_file_name) :  tag_open(false), new_line(true),xml_file_name(_xml_file_name) {
    xml_file.open(xml_file_name);
    assert(xml_file.good());
    xml_file << HEADER;
  }

  xmlwriter::~xmlwriter() {
    xml_file.close();
  }

  xmlwriter& xmlwriter::openElt(string tag) {
    this->closeTag();
    if (elt_stack.size() > 0)
      xml_file << endl;
    indent();
    xml_file << "<" << tag;
    elt_stack.push_back(tag);
    tag_open = true;
    new_line = false;
    return *this;
  }

  xmlwriter& xmlwriter::closeElt() {
    this->closeTag();
     string  elt = elt_stack.back();
     elt_stack.pop_back();
    if (new_line) {
      xml_file << endl;
      indent();
    }
    new_line = true;
    xml_file << "</" << elt << ">";
    return *this;
  }

  xmlwriter& xmlwriter::closeAll() {
    while (elt_stack.size())
      this->closeElt();
    return *this;
  }

  xmlwriter& xmlwriter::attr(string key, string val) {
    xml_file << " " << key << " = \"";
    write_escape(val);
    xml_file << "\"";
    return *this;
  }

  xmlwriter& xmlwriter::attr(string key, const int val) {
    xml_file << " " << key << " = " << val;
    return *this;
  }

  void xmlwriter::close_file() {
    closeAll();
    xml_file << endl << "</xml>" << endl;
    xml_file.close();
  }

  void xmlwriter::closeTag() {
    if (tag_open) {
      xml_file << ">";
      tag_open = false;
    }
  }

   void xmlwriter::indent() {
    for (unsigned int i = 0; i < elt_stack.size(); i++)
      xml_file << "    ";
  }

   void xmlwriter::write_escape(string str) {
      replaceAll(str,"&","&amp;");
      replaceAll(str,"<","&lt;");
      replaceAll(str,">","&gt;");
      replaceAll(str,"\'","&apos;");
      replaceAll(str,"\"","&quot;");
      xml_file << str;
  }

   void xmlwriter::replaceAll(string& str, const string& from, const string& to) {
    if(from.empty())
      return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
      str.replace(start_pos, from.length(), to);
      start_pos += to.length();
    }
  }

