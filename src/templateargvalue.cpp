/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "templateargvalue.h"


TemplateArgValue::TemplateArgValue ( tree & _decl ): decl(_decl),type_class(NULL),int_val(0),has_int_val(false) {
  if ( TREE_CODE(decl) != INTEGER_CST )
    read_typename_arg(decl);
  else
    has_int_val = true;

  string_val =  get_arg_value(decl,true);
}

void TemplateArgValue::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("IntegerConstant");
  xmlw.attr("value",get_value_as_string());
  xmlw.closeElt();
}

void TemplateArgValue::read_typename_arg( tree decl ) {
  tree_code code = TREE_CODE(decl);

  switch( code ) {

  case REAL_TYPE     :
  case BOOLEAN_TYPE  :
  case INTEGER_TYPE: {
    type_class = new  NativeCppType(decl);
    break;
  }


  case POINTER_TYPE: {
    type_class = new PointerType(decl);
    break;
  }



  case RECORD_TYPE: {
    type_class = new ClassType(decl,false);
    break;
  }

  default: {
    warning(0,"Handling template arguments of type %s is not implemented.",get_tree_code_name(code));
  }

  }

}




string TemplateArgValue::get_arg_value( tree decl, bool top ) {
  tree_code code = TREE_CODE(decl);
  string value;


  switch( code ) {

  case   INTEGER_CST: {
    generic_wide_int<wi::extended_tree<192> > v = wi::to_widest(decl);
    int_val = v.to_shwi();
    stringstream ss;
    ss << int_val;
    value = ss.str();
    break;
  }


  case REAL_TYPE     :
  case BOOLEAN_TYPE  :
  case INTEGER_TYPE: {
    string name = lang_hooks.decl_printable_name (TYPE_NAME (decl), 0x0);
    value = name;
    break;
  }


  case POINTER_TYPE: {
    tree type (TREE_TYPE (decl));
    assert( type != error_mark_node );
    tree base_type =  TREE_TYPE (TYPE_POINTER_TO(type));
    value = "ptr_to_" + get_arg_value(base_type,false);
    break;
  }



  case RECORD_TYPE: {
    tree type = TREE_CHAIN(decl);
    string full_name = lang_hooks.decl_printable_name (type, 0x2);
    bool templated = ( ( full_name.find("<") != string::npos ) && (full_name.find(">") != string::npos) );
    string class_name = lang_hooks.decl_printable_name (type, 0); 
    value = class_name;
     if ( templated ) 
       value +=  traverse_nested_args(type);
    break;
  }

  default: {
    warning(0,"Handling template arguments of type %s is not implemented.",get_tree_code_name(code));
  }

  }
    if ( !top )
      value = "_" + value;

  return value;
}


string TemplateArgValue::traverse_nested_args( tree type ) {
  string value;
  tree decl_type = TREE_TYPE(type);
  tree args = TYPE_TI_ARGS (decl_type);
  args = INNERMOST_TEMPLATE_ARGS (args);
  for ( int ix = 0; ix !=  TREE_VEC_LENGTH (args); ix++) {
    tree elt = TREE_VEC_ELT (args, ix);
     value +=  get_arg_value(elt,false);
  }
  return value;
}

void TemplateArgValue::print() {
  cout << string_val << endl;
}

TypeBase * TemplateArgValue::get_value_as_type() {
  return type_class;
}


string TemplateArgValue::get_value_as_string() {
  return string_val;
}

int TemplateArgValue::get_value_as_int() {
  assert(has_int_val);
  return int_val;
}
