/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_TYPENAME_
#define _H_TYPENAME_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "typebase.h"


using namespace std;

class TypeName: public TypeBase {
public:


TypeName ( tree & _type );
TypeBase::t_variant get_variant();
void dump_xml( xmlwriter & xmlw );
void set_tpl_value( TypeBase * v );

private:
  TypeBase * tpl_value;

TypeName();
};

inline TypeName * is_TypeName(TypeBase * n) {
  TypeName * rtn;
  if (  (n->get_variant() == TypeBase::V_TypeName) && ( n != NULL) )
    rtn = static_cast<TypeName*>(n);
  else
    rtn = NULL;
  return rtn;
}



#endif
