/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_ARRAYTYPE_
#define _H_ARRAYTYPE_

#include <iostream>
#include <sstream>
#include <vector>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "typebase.h"
#include "typebase.h"
#include "nativecpptype.h"
#include "classtype.h"
#include "pointertype.h"


using namespace std;

class ArrayType: public TypeBase {
public:


ArrayType ( tree var_decl, tree & _type );
TypeBase::t_variant get_variant();
void dump_xml( xmlwriter & xmlw );

 TypeBase * get_element_type();

private:
vector<unsigned int> dimensions;
int size;
int dim_count;
TypeBase *elt_type;
ArrayType();
};


inline ArrayType * is_ArrayType(TypeBase * n) {
  ArrayType * rtn;
  if (  (n->get_variant() == TypeBase::V_ArrayType) && ( n != NULL) )
    rtn = static_cast<ArrayType*>(n);
  else
    rtn = NULL;
  return rtn;
}

#endif
