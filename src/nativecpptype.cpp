/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "nativecpptype.h"



NativeCppType::NativeCppType ( tree & _type ): TypeBase(_type) {

  if ( TREE_CODE(type) == VOID_TYPE )
    name = "void";
  else
    name = lang_hooks.decl_printable_name (TYPE_NAME (type), 0x0);

  full_name = name;
  line = 0;
  col = 0;
  file = "<builtin>";
}

TypeBase::t_variant NativeCppType::get_variant() {
  return  TypeBase::V_NativeCppType;
}


void NativeCppType::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("NativeCppType");
  xmlw.attr("name",name);
  xmlw.closeElt();

}
