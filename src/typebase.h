/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/
#ifndef _H_TYPEBASE_
#define _H_TYPEBASE_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"


using namespace std;

class TypeBase {
 public:

  enum t_variant {
    V_Undef = 0,
    V_NativeCppType,
    V_ClassType,
    V_PointerType,
    V_ArrayType,
    V_TypeName
  };


  TypeBase ( tree & _type );
  virtual t_variant get_variant() = 0;
  virtual void print();
  virtual void dump_xml( xmlwriter & xmlw ) = 0;

  string get_full_name();
  string get_name();
  string get_scope();
  unsigned int get_line();
  unsigned int get_col();
  string get_file();


 protected:
  tree & type;
  string name;
  string scope;
  string full_name;
  unsigned int line;
  unsigned int col;
  string file;

 private:
  TypeBase();
};

#endif
