/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "arraytype.h"



ArrayType::ArrayType ( tree var_decl, tree & _type ): TypeBase(_type) {
  tree element_type;
  element_type = strip_array_types (type);
  elt_type = NULL;
  if ( element_type  ) {

  tree_code tc = TREE_CODE (element_type);

  switch ( tc ) {
  case BOOLEAN_TYPE:
  case INTEGER_TYPE:
  case REAL_TYPE:
  case VOID_TYPE: {
    elt_type = new NativeCppType(element_type);
    break;
  }

  case POINTER_TYPE: {
    elt_type = new PointerType(element_type);
    break;
  }


  case RECORD_TYPE:  {
    elt_type = new ClassType(element_type,false);
    break;
  }


  default: {
    warning_at(DECL_SOURCE_LOCATION(var_decl),0,"Element type  %s in array variable declaration is not implemented.",get_tree_code_name(tc));
    break;

  }

  }



  }




  full_name = lang_hooks.decl_printable_name (TYPE_NAME (element_type), 0x2);
  size = int_size_in_bytes (type) / int_size_in_bytes (element_type);
  dim_count = 0;

  // TO DO: Fix crashing on some compiler inferred arrays 
 
  // while ( TREE_TYPE (type) != NULL_TREE ) {
  //   tree dom = TYPE_DOMAIN(type);

  //   if ( !dom || dom == error_mark_node ) {
  //     warning_at(DECL_SOURCE_LOCATION (var_decl),0,"Cannot statically resolve array size: %qD.",var_decl); 
  //     return;
  //   }

  //   tree min = TYPE_MIN_VALUE(dom);
  //   tree max = TYPE_MAX_VALUE(dom);
  //   if ( (min != NULL_TREE ) && ( max != NULL_TREE )  ) {
  //     generic_wide_int<wi::extended_tree<192> > v_max = wi::to_widest(max);
  //     unsigned int max_val = v_max.to_shwi();
  //     generic_wide_int<wi::extended_tree<192> > v_min = wi::to_widest(min);
  //     unsigned int min_val = v_min.to_shwi();
  //     dimensions.push_back(min_val);
  //     dimensions.push_back(max_val);
  //   }


  //   type = TREE_TYPE (type);

  //   dim_count++;
  // }

  name = lang_hooks.decl_printable_name (TYPE_NAME (element_type), 0x0);

  for( int n=0; n < dim_count; n++ )
    name += "[]";
  full_name = lang_hooks.decl_printable_name (TYPE_NAME (element_type), 0x2);
  assert( dim_count == dimensions.size()/2 );
  for ( vector<unsigned int>::iterator i= dimensions.begin(); i != dimensions.end(); i+=2 ) {
    int min = *i;
    int max = *(i+1);
    stringstream s;
    s << "[" << max-min+1 << "]";
    full_name += s.str();
  }

  line = 0;
  col = 0;
  file = "<builtin>";


}

TypeBase::t_variant ArrayType::get_variant() {
  return  TypeBase::V_ArrayType;
}


void ArrayType::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("ArrayType");
  xmlw.attr("name",name).attr("full_name",full_name).attr("size",size).attr("dimensions",dim_count);

  int idx =0;
  for ( vector<unsigned int>::iterator i= dimensions.begin(); i != dimensions.end(); i+=2 ) {
    int min = *i;
    int max = *(i+1);
    xmlw.openElt("dimension").attr("index",idx).attr("min",min).attr("max",max).closeElt();
    idx++;
  }

  if( elt_type )
    elt_type->dump_xml(xmlw);

  xmlw.closeElt();

}


TypeBase * ArrayType::get_element_type() {

return elt_type;

}
