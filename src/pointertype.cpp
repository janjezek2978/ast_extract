/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "pointertype.h"


PointerType::PointerType ( tree & _type ): TypeBase(_type) {
  ptr_level = 0;
  // tree base_type =  TREE_TYPE (TYPE_POINTER_TO(TREE_TYPE(type)));
  // while ( TREE_CODE(base_type) == POINTER_TYPE ) {
  //   base_type =  TREE_TYPE (TYPE_POINTER_TO(TREE_TYPE(base_type)));
  //   ptr_level++;
  // }



  // if ( base_type == error_mark_node || base_type == NULL_TREE ) {
  //   name = "unknown";
  //   full_name = name;
  // } else {
  // name = lang_hooks.decl_printable_name (TYPE_NAME (base_type), 0x0);
  //   full_name = lang_hooks.decl_printable_name (TYPE_NAME (base_type), 0x2);
  // }
}

TypeBase::t_variant PointerType::get_variant() {
  return  TypeBase::V_PointerType;
}


void PointerType::dump_xml( xmlwriter & xmlw ) {
  //  for( int n = 0; n < ptr_level+1; n++ ) 
    xmlw.openElt("PointerType");

  // xmlw.openElt("pointer_to_type").attr("name",name).attr("full_name",full_name).closeElt();
  // for( int n = 0; n < ptr_level+1; n++ ) 
    xmlw.closeElt();
}
