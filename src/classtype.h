/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_CLASSTYPE_
#define _H_CLASSTYPE_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "tree-pretty-print.h"

#include "diagnostic.h"

#include "templateargvalue.h"
#include "vardecl.h"
#include "typedecl.h"
#include "functiondecl.h"
#include "xmlwriter.h"
#include "typebase.h"



using namespace std;

class VarDecl;
class TypeDecl;
class FunctionDecl;

class ClassType : public TypeBase {
public:


ClassType ( tree & _type, bool analyze_memebers_en );
TypeBase::t_variant get_variant();
string get_instance_name();
void print();
void dump_xml( xmlwriter & xmlw );
bool is_direct_base_name(const string &n );
bool is_inherits_from(const string & n );
void add_direct_base(ClassType * b );
void add_inherits_from(ClassType * c );
bool is_template();
bool is_module();
bool is_module_base();
void  set_is_module( bool v );
void  set_is_module_base( bool v  );
vector<FunctionDecl *> & get_member_function_decls();

private:

void analyze_memebers(tree decl );


string instance_name;
tree decl;
bool template_flag;
bool module;
bool module_base;
unsigned int ti_arg_count;
vector<TemplateArgValue*> template_arg_values;
vector<ClassType*>   inherits_from;
vector<ClassType*>   direct_bases;
vector<VarDecl*> member_var_decls;
vector<TypeDecl*> member_type_decls;
vector<DeclBase*> template_parm_decls;
vector<FunctionDecl *> member_function_decls;
ClassType();
};



inline ClassType * is_ClassType(TypeBase * n) {
  ClassType * rtn;
  if (  (n->get_variant() == TypeBase::V_ClassType) && ( n != NULL) )
    rtn = static_cast<ClassType*>(n);
  else
    rtn = NULL;
  return rtn;
}

#endif
