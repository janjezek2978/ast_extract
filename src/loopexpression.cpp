/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#include "loopexpression.h"



LoopExpression::LoopExpression ( tree & _type ): ExpressionBase(_type) {

  line = 0;
  col = 0;
  file = "<builtin>";
}

ExpressionBase::t_variant LoopExpression::get_variant() {
  return  ExpressionBase::V_LoopExpression;
}


void LoopExpression::dump_xml( xmlwriter & xmlw ) {
  xmlw.openElt("LoopExpression");
  xmlw.openElt("location").attr("line",line).attr("col",col).attr("file",file).closeElt();
  xmlw.closeElt();

}
