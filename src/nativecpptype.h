/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_NATIVECPPTYPE_
#define _H_NATIVECPPTYPE_

#include <iostream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "coretypes.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "typebase.h"


using namespace std;

class NativeCppType: public TypeBase {
public:


NativeCppType ( tree & _type );
TypeBase::t_variant get_variant();
void dump_xml( xmlwriter & xmlw );


private:


NativeCppType();
};


inline NativeCppType * is_NativeCppType(TypeBase * n) {
  NativeCppType * rtn;
  if (  (n->get_variant() == TypeBase::V_NativeCppType) && ( n != NULL) )
    rtn = static_cast<NativeCppType*>(n);
  else
    rtn = NULL;
  return rtn;
}

#endif
