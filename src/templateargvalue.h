/*   

    © 2019 Cadence Design Systems, Inc. All rights reserved worldwide. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License, found at:
    http://www.gnu.org/licenses/ for more details. 

*/

#ifndef _H_TEMPLATEARGVALUE_
#define _H_TEMPLATEARGVALUE_


#include <iostream>
#include <sstream>
#include "assert.h"
#include "gcc-plugin.h"
#include "system.h"
#include "langhooks.h"
#include "coretypes.h"
#include "cp/cp-tree.h"
#include "tree.h"
#include "diagnostic.h"
#include "xmlwriter.h"
#include "typebase.h"
#include "classtype.h"



using namespace std;

class TemplateArgValue {
 public:


  TemplateArgValue ( tree & _type );
  void dump_xml( xmlwriter & xmlw );
  TypeBase * get_value_as_type();
  string get_value_as_string();
  int get_value_as_int();
  void print();


 private:
  void   read_typename_arg( tree decl );
  string traverse_nested_args(tree type );
  string get_arg_value(tree decl,bool top);

  tree & decl;
  string string_val;
  int int_val;
  unsigned int line;
  unsigned int col;
  string file;
  TypeBase *type_class;
  bool has_int_val;
  TemplateArgValue();
};

#endif
